import "package:flutter/material.dart";
import 'package:flutter_widgets/pages/column_page.dart';
import 'package:flutter_widgets/pages/custom_multi_child_layout_page.dart';
import 'package:flutter_widgets/pages/flow_page.dart';
import 'package:flutter_widgets/pages/grid_page.dart';
import 'package:flutter_widgets/pages/home_page.dart';
import 'package:flutter_widgets/pages/indexed_stack_page.dart';
import 'package:flutter_widgets/pages/list_page.dart';
import 'package:flutter_widgets/pages/row_page.dart';
import 'package:flutter_widgets/pages/sliver_page.dart';
import 'package:flutter_widgets/pages/stack_page.dart';
import 'package:flutter_widgets/pages/stream_builder_page.dart';
import 'package:flutter_widgets/pages/table_page.dart';
import 'package:flutter_widgets/pages/wrap_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
        accentColor: Colors.deepPurple
      ),
      home: HomePage(),
      routes: {
        ListPage.routeName: (context) => ListPage(),
        ColumnPage.routeName: (context) => ColumnPage(),
        RowPage.routeName: (context) => RowPage(),
        StackPage.routeName: (context) => StackPage(),
        GridPage.routeName: (context) => GridPage(),
        TablePage.routeName: (context) => TablePage(),
        CustomMultiChildLayoutPage.routeName: (context) => CustomMultiChildLayoutPage(),
        SliverPage.routeName: (context) => SliverPage(),
        FlowPage.routeName: (context) => FlowPage(),
        WrapPage.routeName: (context) => WrapPage(),
        IndexedStackPage.routeName: (context) => IndexedStackPage(),
        StreamBuilderPage.routeName: (context) => StreamBuilderPage()
      }
    );
  }
}
