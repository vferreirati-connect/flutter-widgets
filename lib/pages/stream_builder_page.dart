import 'dart:async';

import 'package:flutter/material.dart';

class StreamBuilderPage extends StatefulWidget {
  static String routeName = "/streamBuilders";

  @override
  State<StatefulWidget> createState() => _StreamBuilderPageState();
}

class _StreamBuilderPageState extends State<StreamBuilderPage> {
  StreamController<List<Widget>> _controller = StreamController();
  Sink<List<Widget>> get _inAdd => _controller.sink;
  Stream<List<Widget>> get _out => _controller.stream;


  Widget _description() {
    return Text(
      "Widget that builds itself based on the latest snapshot of interaction with a Stream.",
      style: TextStyle(fontSize: 20),
      textAlign: TextAlign.center,
    );
  }

  Widget _listItem(Color color, String name) {
    return ListTile(
      leading: CircleAvatar(
        backgroundColor: color,
        radius: 30,
      ),
      title: Text(name),
    );
  }

  void _loadData() async {
    await Future.delayed(Duration(seconds: 3));

    var items = <Widget>[
      _listItem(Colors.red, "Red"),
      Divider(),
      _listItem(Colors.blue, "Blue"),
      Divider(),
      _listItem(Colors.green, "Green"),
      Divider(),
      _listItem(Colors.yellow, "Yellow"),
      Divider(),
      _listItem(Colors.orange, "Orange"),
      Divider(),
      _listItem(Colors.black, "Black"),
      Divider(),
      _listItem(Colors.pink, "Pink"),
      Divider(),
      _listItem(Colors.grey, "Grey"),
      Divider(),
    ];

    _inAdd.add(items);
  }

  Widget _streamBuilder(BuildContext context) {
    return StreamBuilder<List<Widget>>(
      stream: _out,
      builder: (context, snapshot) {
        if(snapshot.hasData) {
          var items = snapshot.data;

          return Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: items,
              ),
            ),
          );

        } else {
          _loadData();
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("StreamBuilders"),),
      body: Column(
        children: <Widget>[
          _description(),
          Divider(),
          _streamBuilder(context)
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.close();
  }


}
