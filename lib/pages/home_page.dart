import 'package:flutter/material.dart';
import 'package:flutter_widgets/pages/column_page.dart';
import 'package:flutter_widgets/pages/custom_multi_child_layout_page.dart';
import 'package:flutter_widgets/pages/flow_page.dart';
import 'package:flutter_widgets/pages/grid_page.dart';
import 'package:flutter_widgets/pages/indexed_stack_page.dart';
import 'package:flutter_widgets/pages/list_page.dart';
import 'package:flutter_widgets/pages/row_page.dart';
import 'package:flutter_widgets/pages/sliver_page.dart';
import 'package:flutter_widgets/pages/stack_page.dart';
import 'package:flutter_widgets/pages/stream_builder_page.dart';
import 'package:flutter_widgets/pages/table_page.dart';
import 'package:flutter_widgets/pages/wrap_page.dart';

class HomePage extends StatelessWidget {
  Widget _rowItem(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text("Row"),
          subtitle: Text("Layout a list of child widgets in the horizontal direction."),
          onTap: () => Navigator.of(context).pushNamed(RowPage.routeName),
        ),
        Divider()
      ]
    );
  }

  Widget _columnItem(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text("Column"),
          subtitle: Text("Layout a list of child widgets in the vertical direction."),
          onTap: () => Navigator.of(context).pushNamed(ColumnPage.routeName),
        ),
        Divider()
      ]
    );
  }

  Widget _stackItem(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text("Stack"),
          subtitle: Text("This class is useful if you want to overlap several children in a simple way, for example having some text and an image, overlaid with a gradient and a button attached to the bottom."),
          onTap: () => Navigator.of(context).pushNamed(StackPage.routeName),
        ),
        Divider()
      ],
    );
  }

  Widget _gridItem(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text("GridView"),
          subtitle: Text("A grid list consists of a repeated pattern of cells arrayed in a vertical and horizontal layout. The GridView widget implements this component."),
          onTap: () => Navigator.of(context).pushNamed(GridPage.routeName),
        ),
        Divider()
      ],
    );
  }

  Widget _tableItem(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text("Table"),
          subtitle: Text("A widget that uses the table layout algorithm for its children."),
          onTap: () => Navigator.of(context).pushNamed(TablePage.routeName),
        ),
        Divider()
      ],
    );
  }

  Widget _listItem(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text("ListView"),
          subtitle: Text("A scrollable, linear list of widgets. ListView is the most commonly used scrolling widget. It displays its children one after another in the scroll direction. In the cross axis, the children are required to fill the ListView."),
          onTap: () => Navigator.of(context).pushNamed(ListPage.routeName),
        ),
        Divider()
      ],
    );
  }

  Widget _flowItem(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text("Flow"),
          subtitle: Text("A widget that implements the flow layout algorithm."),
          onTap: () => Navigator.of(context).pushNamed(FlowPage.routeName),
        ),
        Divider()
      ],
    );
  }

  Widget _sliverItem(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text("Sliver"),
          subtitle: Text("A sliver is a portion of a scrollable area. You can use slivers to achieve custom scrolling effects."),
          onTap: () => Navigator.of(context).pushNamed(SliverPage.routeName),
        ),
        Divider()
      ],
    );
  }

  Widget _indexedStackItem(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text("IndexedStack"),
          subtitle: Text("A Stack that shows a single child from a list of children."),
          onTap: () => Navigator.of(context).pushNamed(IndexedStackPage.routeName),
        ),
        Divider()
      ],
    );
  }

  Widget _wrapItem(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text("Wrap"),
          subtitle: Text("A widget that displays its children in multiple horizontal or vertical runs."),
          onTap: () => Navigator.of(context).pushNamed(WrapPage.routeName),
        ),
        Divider()
      ],
    );
  }

  Widget _customMultiChildLayoutItem(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text("CustomMultiChildLayout"),
          subtitle: Text("A widget that uses a delegate to size and position multiple children."),
          onTap: () => Navigator.of(context).pushNamed(CustomMultiChildLayoutPage.routeName),
        ),
        Divider()
      ],
    );
  }

  Widget _streamBuilderItem(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text("StreamBuilder"),
          subtitle: Text("Widget that builds itself based on the latest snapshot of interaction with a Stream."),
          onTap: () => Navigator.of(context).pushNamed(StreamBuilderPage.routeName),
        ),
        Divider()
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter Layouts"),
      ),
      body: ListView(
       children: <Widget>[
         _rowItem(context),
         _columnItem(context),
         _stackItem(context),
         _gridItem(context),
         _tableItem(context),
         _listItem(context),
         _flowItem(context),
         _sliverItem(context),
         _wrapItem(context),
         _streamBuilderItem(context),
         _customMultiChildLayoutItem(context),
         _indexedStackItem(context)
       ],
      )
    );
  }
}
