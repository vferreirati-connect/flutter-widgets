import 'package:flutter/material.dart';

class StackPage extends StatelessWidget {
  static final String routeName = "/stacks";

  Widget _description() {
    return Text(
      "This class is useful if you want to overlap several children in a simple way, for example having some text and an image, overlaid with a gradient and a button attached to the bottom.",
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: 20),
    );
  }

  Widget _stack() {
    return Container(
      height: 250,
      padding: EdgeInsets.all(10),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: AlignmentDirectional.topStart,
            child: CircleAvatar(
              backgroundColor: Colors.red,
              radius: 25,
            ),
          ),
          Align(
            alignment: AlignmentDirectional.topCenter,
            child: CircleAvatar(
              backgroundColor: Colors.green,
              radius: 25,
            ),
          ),
          Align(
            alignment: AlignmentDirectional.topEnd,
            child: CircleAvatar(
              backgroundColor: Colors.yellow,
              radius: 25,
            ),
          ),
          Align(
            alignment: AlignmentDirectional.center,
            child: CircleAvatar(
              backgroundColor: Colors.blue,
              radius: 25,
            ),
          ),
          Align(
            alignment: AlignmentDirectional.bottomStart,
            child: CircleAvatar(
              backgroundColor: Colors.purple,
              radius: 25,
            ),
          ),
          Align(
            alignment: AlignmentDirectional.bottomCenter,
            child: CircleAvatar(
              backgroundColor: Colors.grey,
              radius: 25,
            ),
          ),
          Align(
            alignment: AlignmentDirectional.bottomEnd,
            child: CircleAvatar(
              backgroundColor: Colors.teal,
              radius: 25,
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Stacks")),
      body: Column(
        children: <Widget>[
          _description(),
          Divider(),
          _stack()
        ],
      ),
    );
  }
}
