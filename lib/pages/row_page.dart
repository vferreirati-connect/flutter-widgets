import 'package:flutter/material.dart';

class RowPage extends StatelessWidget {
  static String routeName = "/rows";
  
  Widget _description() {
    return Center(
      child: Text(
        "Layout a list of child widgets in the horizontal direction.",
        style: TextStyle(fontSize: 20),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _row() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(width: 10,),
          CircleAvatar(backgroundColor: Colors.red, radius: 45),
          SizedBox(width: 10,),
          CircleAvatar(backgroundColor: Colors.deepPurple, radius: 45),
          SizedBox(width: 10,),
          CircleAvatar(backgroundColor: Colors.deepOrange, radius: 45),
          SizedBox(width: 10,),
          CircleAvatar(backgroundColor: Colors.grey, radius: 45),
          SizedBox(width: 10,),
          CircleAvatar(backgroundColor: Colors.blue, radius: 45),
          SizedBox(width: 10,),
          CircleAvatar(backgroundColor: Colors.yellow, radius: 45),
          SizedBox(width: 10,),
          CircleAvatar(backgroundColor: Colors.green, radius: 45),
          SizedBox(width: 10,),
          CircleAvatar(backgroundColor: Colors.amber, radius: 45),
          SizedBox(width: 10,),
          CircleAvatar(backgroundColor: Colors.black, radius: 45),
          SizedBox(width: 10,),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Rows")),
      body: Container(
        child: Column(
          children: <Widget>[
            _description(),
            Divider(),
            SizedBox(height: 20),
            _row()
          ],
        ),
      ),
    );
  }
}
