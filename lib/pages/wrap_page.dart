import 'package:flutter/material.dart';

class WrapPage extends StatelessWidget {
  static String routeName = "/wraps";

  Widget _description() {
    return Center(
      child: Text(
        "A widget that displays its children in multiple horizontal or vertical runs.",
        style: TextStyle(fontSize: 20),
        textAlign: TextAlign.center,
      )
    );
  }
  
  Widget _wrap() {
    return Wrap(
      direction: Axis.horizontal,
      runSpacing: 10,
      spacing: 10,
      children: <Widget>[
        Chip(
          avatar: CircleAvatar(
            backgroundColor: Colors.red,
            radius: 20,
          ),
          label: Text("Red"),
        ),
        Chip(
          avatar: CircleAvatar(
            backgroundColor: Colors.blue,
            radius: 20,
          ),
          label: Text("Blue"),
        ),
        Chip(
          avatar: CircleAvatar(
            backgroundColor: Colors.green,
            radius: 20,
          ),
          label: Text("Green"),
        ),
        Chip(
          avatar: CircleAvatar(
            backgroundColor: Colors.purple,
            radius: 20,
          ),
          label: Text("purple"),
        ),
        Chip(
          avatar: CircleAvatar(
            backgroundColor: Colors.orange,
            radius: 20,
          ),
          label: Text("Orange"),
        ),
        Chip(
          avatar: CircleAvatar(
            backgroundColor: Colors.amber,
            radius: 20,
          ),
          label: Text("Amber"),
        ),
        Chip(
          avatar: CircleAvatar(
            backgroundColor: Colors.pink,
            radius: 20,
          ),
          label: Text("Pink"),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Wraps"),),
      body: Container(
        child: Column(
          children: <Widget>[
            _description(),
            Divider(),
            _wrap()
          ],
        ),
      ),
    );
  }
}
