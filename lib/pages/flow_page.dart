import 'package:flutter/material.dart';

class FlowPage extends StatelessWidget {
  static String routeName = "/flows";

  Widget _description() {
    return Center(
        child: Text(
          "A widget that implements the flow layout algorithm. Flow layouts are optimized for moving children around the screen using transformation matrices. ",
          style: TextStyle(fontSize: 20),
          textAlign: TextAlign.center,
        )
    );
  }

  Widget _listItem(Color color) {
    return CircleAvatar(
      backgroundColor: color,
      radius: 20,
    );
  }

  Widget _flow() {
    var items = <Widget>[
      _listItem(Colors.red),
      _listItem(Colors.blue),
      _listItem(Colors.green),
    ];

    return Flow(
      delegate: _MyFlowDelegate(),
      children: items,
    );
  }

  @override  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Flows"),),
      body: Container(
        child: Column(
          children: <Widget>[
            _description(),
            Divider(),
            Expanded(child: Container(child: _flow()))
          ],
        ),
      ),
    );
  }
}

class _MyFlowDelegate extends FlowDelegate {
  @override
  void paintChildren(FlowPaintingContext context) {
    var count = context.childCount;
    var width = context.size.width;
    var height = context.size.height;

    print("Size is $width x $height");
    for(int i=0; i<count; i++) {
      context.paintChild(
        i,
        transform: Matrix4.translationValues(50.0 * i, 0.0, 0.0)
      );
    }
  }

  @override
  bool shouldRepaint(FlowDelegate oldDelegate) => false;
}
