import 'package:flutter/material.dart';

class ListPage extends StatelessWidget {
  static final String routeName = "/lists";

  Widget _description() {
    return Text(
      "A scrollable, linear list of widgets. ListView is the most commonly used scrolling widget. It displays its children one after another in the scroll direction. In the cross axis, the children are required to fill the ListView.",
      style: TextStyle(fontSize: 20),
      textAlign: TextAlign.center,
    );
  }

  Widget _listItem(Color color, String name) {
    return ListTile(
      leading: CircleAvatar(
        backgroundColor: color,
        radius: 30,
      ),
      title: Text(name),
    );
  }

  Widget _list() {
    return ListView(
      padding: EdgeInsets.all(10),
      children: <Widget>[
        _listItem(Colors.red, "Red"),
        Divider(),
        _listItem(Colors.blue, "Blue"),
        Divider(),
        _listItem(Colors.green, "Green"),
        Divider(),
        _listItem(Colors.yellow, "Yellow"),
        Divider(),
        _listItem(Colors.orange, "Orange"),
        Divider(),
        _listItem(Colors.black, "Black"),
        Divider(),
        _listItem(Colors.pink, "Pink"),
        Divider(),
        _listItem(Colors.grey, "Grey"),
        Divider(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("ListViews")),
      body: Column(
        children: <Widget>[
          _description(),
          Divider(),
          Expanded(child: _list())
        ],
      ),
    );
  }
}
