import 'package:flutter/material.dart';

class IndexedStackPage extends StatefulWidget {
  static String routeName = "/indexedStacks";

  @override
  State createState() => _IndexedPageState();
}

class _IndexedPageState extends State<IndexedStackPage> {
  var icons = [
    Icon(Icons.add, color: Colors.blue, size: 80,),
    Icon(Icons.print, color: Colors.purple, size: 80,),
    Icon(Icons.inbox, color: Colors.orange, size: 80,),
    Icon(Icons.launch, color: Colors.pink, size: 80,),
    Icon(Icons.message, color: Colors.grey, size: 80,)
  ];

  int index;

  @override
  void initState() {
    super.initState();
    index = 0;
  }

  void _onIncrement() {
    setState(() {
      index = (index + 1) % icons.length;
    });
  }

  Widget _description() {
    return Text(
      "A Stack that shows a single child from a list of children.\nPress the floating action button to increment the index value",
      style: TextStyle(fontSize: 20),
      textAlign: TextAlign.center,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("IndexedStacks"),),
      body: Container(
        child: Column(
          children: <Widget>[
            _description(),
            Divider(),
            IndexedStack(
              children: icons,
              index: index,
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _onIncrement,
        child: Icon(Icons.add),
      ),
    );
  }
}
