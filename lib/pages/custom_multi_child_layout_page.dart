import 'package:flutter/material.dart';

class CustomMultiChildLayoutPage extends StatelessWidget {
  static String routeName = "/customMultiChildLayouts";

  Widget _description() {
    return Text(
      "A widget that uses a delegate to size and position multiple children.",
      style: TextStyle(fontSize: 20),
      textAlign: TextAlign.center,
    );
  }

  Widget _listItem(Color color) {
    return CircleAvatar(
      backgroundColor: color,
    );
  }

  Widget _customMultiChildLayout() {
    var items = <Widget>[
      LayoutId(id: "item0", child: _description()),
      LayoutId(id: "item1", child: _listItem(Colors.red)),
      LayoutId(id: "item2", child: _listItem(Colors.blue)),
      LayoutId(id: "item3", child: _listItem(Colors.green)),
    ];

    return CustomMultiChildLayout(
      delegate: _MyLayoutDelegate(itemCount: items.length, circleRadius: 20),
      children: items,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("CustomMultiChildLayouts"),),
      body: _customMultiChildLayout(),
    );
  }
}

class _MyLayoutDelegate extends MultiChildLayoutDelegate {

  int itemCount;
  double circleRadius;

  _MyLayoutDelegate({@required this.itemCount, @required this.circleRadius});

  @override
  void performLayout(Size size) {
    print("Layout size is ${size.width}x${size.height}");

    var descriptionSize = layoutChild("item0", BoxConstraints.loose(size));
    positionChild("item0", Offset.zero);
    print("Description size is ${descriptionSize.width}x${descriptionSize.height}");
    print(size.height - descriptionSize.height + 20);

    for(int i=1; i<itemCount; i++) {
      var childId = "item$i";
      layoutChild(childId, BoxConstraints.tight(Size(circleRadius * 2, circleRadius * 2)));
      positionChild("item$i", Offset(
          size.width / 2 - circleRadius,
          (descriptionSize.height + circleRadius) * (i)
      ));
    }
  }

  @override
  bool shouldRelayout(MultiChildLayoutDelegate oldDelegate) => false;
}
