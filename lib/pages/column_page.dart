import 'package:flutter/material.dart';

class ColumnPage extends StatelessWidget {
  static final String routeName = "/columns";

  Widget _description(BuildContext context) {
    return Center(
      child: Text(
        "Layout a list of child widgets in the vertical direction.",
        style: TextStyle(fontSize: 20),
        textAlign: TextAlign.center,
      )
    );
  }

  Widget _column() {
    return Column(
      children: <Widget>[
        SizedBox(height: 10,),
        CircleAvatar(backgroundColor: Colors.red, radius: 45),
        SizedBox(height: 10,),
        CircleAvatar(backgroundColor: Colors.deepPurple, radius: 45),
        SizedBox(height: 10,),
        CircleAvatar(backgroundColor: Colors.deepOrange, radius: 45),
        SizedBox(height: 10,),
        CircleAvatar(backgroundColor: Colors.grey, radius: 45),
        SizedBox(height: 10,),
        CircleAvatar(backgroundColor: Colors.blue, radius: 45),
        SizedBox(height: 10,),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Columns")),
      body: Container(
        child: Column(
          children: <Widget>[
            _description(context),
            Divider(),
            Expanded(
              child: SingleChildScrollView(
                  child: _column()
              )
            )
          ],
        ),
      ),
    );
  }
}
