import 'package:flutter/material.dart';

class TablePage extends StatelessWidget {
  static final String routeName = "/tables";

  Widget _description() {
    return Text(
      "A widget that uses the table layout algorithm for its children.",
      style: TextStyle(fontSize: 20),
      textAlign: TextAlign.center,
    );
  }

  Widget _table() {
    return Table(
      border: TableBorder.all(color: Colors.deepPurple, width: 1),
      children: <TableRow>[
        TableRow(
          children: <Widget>[
            TableCell(
              verticalAlignment: TableCellVerticalAlignment.middle,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(backgroundColor: Colors.red, radius: 15),
              )
            ),
            TableCell(
              verticalAlignment: TableCellVerticalAlignment.middle,
              child: Center(child: Text("Red"))
            )
          ]
        ),
        TableRow(
            children: <Widget>[
              TableCell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CircleAvatar(backgroundColor: Colors.blue, radius: 15),
                )
              ),
              TableCell(
                verticalAlignment: TableCellVerticalAlignment.middle,
                child: Center(child: Text("Blue"))
              )
            ]
        ),
        TableRow(
            children: <Widget>[
              TableCell(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CircleAvatar(backgroundColor: Colors.yellow, radius: 15),
                  )
              ),
              TableCell(
                  verticalAlignment: TableCellVerticalAlignment.middle,
                  child: Center(child: Text("Yellow"))
              )
            ]
        ),
        TableRow(
          children: <Widget>[
            TableCell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CircleAvatar(backgroundColor: Colors.green, radius: 15),
                )
            ),
            TableCell(
                verticalAlignment: TableCellVerticalAlignment.middle,
                child: Center(child: Text("Green"))
            )
          ]
        ),
        TableRow(
          children: <Widget>[
            TableCell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CircleAvatar(backgroundColor: Colors.purple, radius: 15),
                )
            ),
            TableCell(
                verticalAlignment: TableCellVerticalAlignment.middle,
                child: Center(child: Text("Purple"))
            )
          ]
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Tables"),),
      body: Column(
        children: <Widget>[
          _description(),
          Divider(),
          _table()
        ],
      ),
    );
  }
}