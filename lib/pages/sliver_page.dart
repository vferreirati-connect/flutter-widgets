import 'package:flutter/material.dart';

class SliverPage extends StatelessWidget {
  static String routeName = "/slivers";

  Widget _description() {
    return Center(
        child: Text(
      "A sliver is a portion of a scrollable area. You can use slivers to achieve custom scrolling effects.",
      style: TextStyle(fontSize: 20),
      textAlign: TextAlign.center,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              title: Text("Slivers"),
              floating: true,
            ),
            SliverList(
              delegate: SliverChildListDelegate([
                _description(),
                Divider(),
                Container(
                  color: Colors.red,
                  height: 100,
                ),
                Container(
                  color: Colors.blue,
                  height: 100,
                ),
                Container(
                  color: Colors.yellow,
                  height: 100,
                ),
                Container(
                  color: Colors.red,
                  height: 100,
                ),
                Container(
                  color: Colors.blue,
                  height: 100,
                ),
                Container(
                  color: Colors.yellow,
                  height: 100,
                ),Container(
                  color: Colors.red,
                  height: 100,
                ),
                Container(
                  color: Colors.blue,
                  height: 100,
                ),
                Container(
                  color: Colors.yellow,
                  height: 100,
                ),
              ])
            )
          ],
        ),
      ),
    );
  }
}
