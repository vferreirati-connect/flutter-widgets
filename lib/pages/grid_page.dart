import 'package:flutter/material.dart';

class GridPage extends StatelessWidget {
  static final String routeName = "/grids";

  Widget _description() {
    return Text(
      "A grid list consists of a repeated pattern of cells arrayed in a vertical and horizontal layout. The GridView widget implements this component.",
      style: TextStyle(fontSize: 20),
      textAlign: TextAlign.center,
    );
  }

  Widget _grid() {
    return GridView.count(
      primary: true,
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      shrinkWrap: false,
      padding: EdgeInsets.all(10),
      crossAxisCount: 3,
      children: <Widget>[
        CircleAvatar(backgroundColor: Colors.red, radius: 10),
        CircleAvatar(backgroundColor: Colors.deepPurple, radius: 10),
        CircleAvatar(backgroundColor: Colors.deepOrange, radius: 10),
        CircleAvatar(backgroundColor: Colors.grey, radius: 10),
        CircleAvatar(backgroundColor: Colors.blue, radius: 10),
        CircleAvatar(backgroundColor: Colors.yellow, radius: 10),
        CircleAvatar(backgroundColor: Colors.green, radius: 10),
        CircleAvatar(backgroundColor: Colors.amber, radius: 10),
        CircleAvatar(backgroundColor: Colors.black, radius: 10),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Grids")),
      body: Column(
        children: <Widget>[
          _description(),
          Divider(),
          Expanded(child: _grid())
        ],
      ),
    );
  }
}
